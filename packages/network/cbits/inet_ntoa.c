/* Copyright 2020 Nico Sonack
 *
 * Needed to avoid linker errors.
 */

#include "HsNet.h"

char *
my_inet_ntoa(
#if defined(HAVE_WINSOCK_H)
	     u_long addr
#elif defined(HAVE_IN_ADDR_T)
	     in_addr_t addr
#elif defined(HAVE_INTTYPES_H)
	     u_int32_t addr
#else
	     unsigned long addr
#endif
	     )
{
    struct in_addr a;
    a.s_addr = addr;
    return inet_ntoa(a);
}
